class CreateMemoirs < ActiveRecord::Migration
  def change
    create_table :memoirs do |t|
      t.string :lang
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
