class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.date :start
      t.date :finish
      t.string :school
      t.string :proffesion
      t.string :speciality
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
