class MemoirsExperiences < ActiveRecord::Migration
  def self.up
    create_table 'memoirs_experiences', :id => false do |t|
      t.column :memoir_id, :integer
      t.column :experience_id, :integer
    end
  end
end
