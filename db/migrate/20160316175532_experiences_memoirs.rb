class ExperiencesMemoirs < ActiveRecord::Migration
  def self.up
    create_table 'experiences_memoirs', :id => false do |t|
      t.column :memoir_id, :integer
      t.column :experience_id, :integer
    end
  end
  def self.down
    drop_table 'experiences_memoirs'

  end
end
