class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.date :start
      t.date :finish
      t.string :firm
      t.string :position
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
