class AddFieldstocEduAndExper < ActiveRecord::Migration
  def change
    add_column :experiencizations, :memoir_id, :integer
    add_column :experiencizations, :experience_id, :integer
    add_column :educatizations, :memoir_id, :integer
    add_column :educatizations, :education_id, :integer
  end
end
