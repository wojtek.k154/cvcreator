class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :type
      t.string :street
      t.string :housenumber
      t.string :flatnumber
      t.string :postalcode
      t.string :city
      t.string :country
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
