class EducationsMemoirs < ActiveRecord::Migration
  def self.up
    create_table 'educations_memoirs', :id => false do |t|
      t.column :memoir_id, :integer
      t.column :education_id, :integer
    end
  end
  def self.down
    drop_table 'educations_memoirs'
    
  end
end
