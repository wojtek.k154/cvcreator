class Droptables < ActiveRecord::Migration
  def change
      drop_table 'memoirs_educations'
      drop_table 'memoirs_experiences'
  end
end
