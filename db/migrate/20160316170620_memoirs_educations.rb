class MemoirsEducations < ActiveRecord::Migration
  def self.up
    create_table 'memoirs_educations', :id => false do |t|
      t.column :memoir_id, :integer
      t.column :education_id, :integer
    end
  end
 
end
