class Droptabless < ActiveRecord::Migration
  def change
    drop_table 'experiences_memoirs'
    drop_table 'educations_memoirs'
  end
end
