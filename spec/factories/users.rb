FactoryGirl.define do
  factory :user do
    email 'foo123@gmail.com'
    password 'foobar1234'
    trait :full_data do
      names 'Jan Adam'
      surname 'Grzesiuk'
      birthdate '23-09-1992'.to_date 
      phone '665998558'
      nationality 'POLAND'
    end
    trait :wrong_phone do
      names 'Jan Adam'
      surname 'Grzesiuk'
      birthdate{ Date.today - 8000.days }
      phone '+48 665 998 558'
      nationality 'POLAND'
    end
  end
end
