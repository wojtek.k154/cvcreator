require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:user, :full_data) }
  describe User do
    it 'has email' do
      expect(user.email).to eq('foo123@gmail.com')
    end

    it 'has names' do
      expect(user.names).to eq('Jan Adam')
    end
    it 'has surname' do
      expect(user.surname).to eq('Grzesiuk')
    end
    it 'has proper phone number' do
      expect(user.phone).to eq('665998558')
    end
    it 'has birthdate' do
      expect(user.birthdate).to eq('23-09-1992'.to_date)
    end
     
  end
end
