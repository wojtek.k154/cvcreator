var app = angular.module('myApp', ['rails', 'ngResource','ngUpload']);

app.run(function($compile, $rootScope, $document) {
	return $document.on('page:load', function(){
		var body, finish;
		body = angular.element('body');
		finish = $compile(body.html())($rootScope);
		return body.html(finish);
	});
});
