app.controller("AddressController",['$scope', 'Address', function($scope, Address){
   $scope.addresses = Address.query();
   $scope.newAddress = { };
   $scope.addAddress = function(){
     Address.save($scope.newAddress).$promise.then(
       function(result){
         $scope.addresses.push(result);
         $scope.newAddress = {};
       }
     )
   }
}]);
