app.factory("Address", function ($resource) {
  return $resource("/addresses/:id.json", {id: "@id"}, {
    'update': {method:"PUT"}
  })
});
