app.controller('PhotoController',['$scope', 'Photo', function($scope, Photo) {
      //  $scope.uploader = new FileUploader({url: '/photos.json', method: 'POST'});
      $scope.photos = Photo.query();
      $scope.newPhoto;
      $scope.addPhotos = function(photos){
        console.log(photos);
        Photo.save(photos).$promise.then(
          function(result){
            console.log(result);
            $scope.photos.push(result);
          },
          function(error) {
            $scope.error = error;
          }
        )
      };
    }]);
