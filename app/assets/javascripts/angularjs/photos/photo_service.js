app.factory("Photo", function ($resource) {
  return $resource("/photos/:id.json", {id: "@id"}, {
    'update': {method:"PUT"}
  })
});
