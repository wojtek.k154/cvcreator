app.factory("Experience", function ($resource) {
  return $resource("/experiences/:id.json", {id: "@id"}, {
    'update': {method:"PUT"}
  })
});
