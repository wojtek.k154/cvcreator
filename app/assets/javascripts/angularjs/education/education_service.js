app.factory("Education", function ($resource) {
  return $resource("/educations/:id.json", {id: "@id"}, {
    'update': {method:"PUT"}
  })
});
