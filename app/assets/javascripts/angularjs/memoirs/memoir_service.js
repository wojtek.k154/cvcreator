app.factory("Memoir", function ($resource) {
  return $resource("/memoirs/:id.json", {id: "@id"}, {
    'update': {method:"PUT"}
  })
});
