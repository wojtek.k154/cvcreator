app.controller("MemoirController", ['$scope', 'Memoir','Address', 'Education', 'Experience', 'Photo',
    function($scope, Memoir, Address, Education, Experience, Photo){
      $scope.memoirs = Memoir.query();
      $scope.newMemoir = { lang: "POLISH" };

      $scope.addMemoirs = function(){
        Memoir.save($scope.newMemoir).$promise.then(
          function(result){
            $scope.memoirs.push(result);
            $scope.newMemoir = {lang: "POLISH"};
          },
          function(err){
            $scope.error = err;
          }
        )
      };
      $scope.deleteMemoirs = function(ID, index){
        Memoir.delete({id: ID, flag: "true"}).$promise.then(
          function(result){
            $scope.memoirs.splice(index, 1);
          },
          function(err){
            $scope.error = err;
          }
        )
      };
    }])
    .controller("MemoirCreateController", ['$scope', 'Memoir','Address', 'Education', 'Experience', 'Photo',
        function($scope, Memoir, Address, Education, Experience, Photo){


          $scope.memoir = Memoir.get({id: gon.memoir.id});
          $scope.educations = Education.query({memoir_id: gon.memoir.id, b: 'false'});
          $scope.memoirEducations = Education.query({memoir_id: gon.memoir.id, b: 'true'});
          $scope.experiences = Experience.query({memoir_id: gon.memoir.id, b: 'false'});
          $scope.memoirExperiences = Experience.query({memoir_id: gon.memoir.id, b: 'true'});
          $scope.addresses = Address.query();
          $scope.newAddress = {};
          $scope.newExperience = {memoir_id: $scope.memoir.id};
          $scope.newEducation = {memoir_id: gon.memoir.id};

          $scope.addEducation = function(){
            Education.save($scope.newEducation).$promise.then(
              function(result){
                $scope.memoirEducations.push(result);
                $scope.newEducation = {memoir_id: $scope.memoir.id};
              }
            )
          };


          $scope.addAddress = function(){
            Address.save($scope.newAddress).$promise.then(
              function(result){
                $scope.addresses.push(result);
                $scope.newAddress = {};
                $scope.memoir = Memoir.update({id: $scope.memoir.id}, {address_id: result.id});
              }
            )
          };


          $scope.addExperience = function(){
            Experience.save($scope.newExperience).$promise.then(
              function(result){
                $scope.memoirExperiences.push(result);
                $scope.newExperience = {memoir_id: $scope.memoir.id};
              }
            )
          };

          $scope.addExperienceToMemoir = function(memoir, exp, index) {
            Experience.update({id: exp}, {memoir_id: memoir}).$promise.then(
              function(success){
                $scope.experiences.splice(index, 1);
                $scope.memoirExperiences.push(success);
              }
            )
          };


          $scope.addEducationToMemoir = function(memoir, edu, index) {
            Education.update({id: edu}, { memoir_id: $scope.memoir}).$promise.then(
              function(success){
                $scope.educations.splice(index, 1);
                $scope.memoirEducations.push(success);

              }
            )
          };


          $scope.updateAddAddress = function(address){
            $scope.memoir = Memoir.update({id: $scope.memoir.id}, { address_id: address});
          };

          $scope.updateMemoir = function(){
            $scope.memoir = Memoir.update({id: $scope.memoir.id}, $scope.memoir);
          };

          $scope.deleteEduFromForm = function(memoir, edu, index){
            Memoir.delete({id: memoir, education_id: edu}).$promise.then(
              function(result){
                $scope.memoirEducations.splice(index, 1);
              } 
            )
          };

          $scope.deleteExpFromForm = function(memoir, exp, index){
            Memoir.delete({id: memoir, experience_id: edu}).$promise.then(
              function(result){
                $scope.memoirExperiences.splice(index, 1);
              }
            )
          };
        //  $scope.

    }])
    app.controller("MemoirShowController", ['$scope', 'Memoir','Address', 'Education', 'Experience', 'Photo',
        function($scope, Memoir, Address, Education, Experience, Photo){
          $scope.memoir = gon.memoir;
          $scope.address = Address.get({id: $scope.memoir.address_id})
          $scope.memoirEducations = Education.query({memoir_id: gon.memoir.id, b: 'true'});
          $scope.memoirExperiences = Experience.query({memoir_id: gon.memoir.id, b: 'true'});

        }]);
