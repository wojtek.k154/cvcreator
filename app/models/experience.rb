class Experience < ActiveRecord::Base
  belongs_to :user
  has_many :experiencizations
  has_many :memoirs, through: :experiencizations
  validates :start, presence: true
  validates :finish, presence: true
  validates :firm, presence: true
  validates :position, presence: true
  validates :user_id, presence: true
end
