class Education < ActiveRecord::Base
  belongs_to :user
  has_many :educatizations
  has_many :memoirs, through: :educatizations
  validates :start, presence: true
  validates :finish, presence: true
  validates :school, presence: true
  validates :proffesion, presence: true
  validates :speciality, presence: true
  validates :user_id, presence: true
end
