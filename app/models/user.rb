class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :addresses
  has_many :photos
  has_many :memoirs
  has_many :educations
  has_many :experiences

  validates :names, presence: true
  validates :surname, presence: true
  validates :birthdate, presence: true
  validates :phone, presence: true,
                    :length => { :minimum => 7, :maximum => 15 }
   

end
