class Memoir < ActiveRecord::Base
  belongs_to :user
  belongs_to :address
  has_many :educatizations
  has_many :educations, through: :educatizations
  has_many :experiencizations
  has_many :experiences, through: :experiencizations

  validates :lang, presence: true
  validates :user_id, presence: true,
                   :numericality => true
end
