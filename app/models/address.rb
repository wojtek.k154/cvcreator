class Address < ActiveRecord::Base
  belongs_to :user
  has_many :memoirs
  validates :street, presence: true

  validates :housenumber, presence: true

  validates :postalcode, presence: true
  validates :city, presence: true
  validates :country, presence: true
  validates :user_id, presence: true
end
