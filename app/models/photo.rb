class Photo < ActiveRecord::Base
  belongs_to :user

  has_attached_file :img, styles: { medium: "400x300>", thumb: "200x200>",small:"100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :img, content_type: /\Aimage\/.*\Z/

  def as_json(options={})
    hash = super(options)
    hash.merge!(
      'img_medium_url' => img.url(:medium),
      'img_thumb_url' => img.url(:thumb),
      'img_small_url' => img.url(:small),
      'img_url' => img.url,
    )
    hash
  end
end
