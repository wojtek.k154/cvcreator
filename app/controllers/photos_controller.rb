class PhotosController < ApplicationController
  before_action :set_photo, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token
  def index
    @photos  = current_user.photos.order('created_at DESC')
    respond_to do |format|
      format.json { render json: @photos }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @photo }
      format.html
    end
  end

  def create
    @photo = current_user.photos.create!(img: params[:img])
    respond_to do |format|
      format.html
      format.json { render json: @photo }
    end
  end

  def destroy
    @photo.destroy
    respond_to do |format|
      format.json { render json: @photo }
    end
  end

  private
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def photo_params
      params.require(:photo).permit(:img, :user_id)
    end
end
