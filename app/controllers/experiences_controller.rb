class ExperiencesController < ApplicationController
  before_action :set_experience, only: [:show, :update, :destroy]
  def index
    #binding.pry
    if params[:memoir_id] && params[:b] == "true"
      memoir = Memoir.find(params[:memoir_id])
      @experiences = memoir.experiences.order("created_at DESC")
    elsif  params[:memoir_id] && params[:b] == "false"
      @experiences = current_user.experiences.order("created_at DESC").select { |u| !u.memoir_ids.include?(params[:memoir_id]) }
    end

    respond_to do |format|
      format.json { render json: @experiences }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @experience }
      format.html
    end
  end

  def create
    @experience = current_user.experiences.new(experience_params)
    respond_to do |format|
      if @experience.save
        format.json { render json: @experience }
      else
        format.json { render json: @experience.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @experience.update(memoir_ids: params[:memoir_id])
        format.json { render json: @experience }
      else
        format.json { render json: @experience.errors, :status => :unprocessable_entity }
      end
    end
  end
  def destroy
    @experience.destroy
    respond_to do |format|
      format.json { render json: @experience }
    end
  end
  private
    def set_experience
      @experience = Experience.find(params[:id])
    end

    def experience_params
      params.require(:experience).permit(:start, :finish, :firm, :position, :user_id, :memoir_ids)
    end
end
