class MemoirsController < ApplicationController
  before_action :set_memoir, only: [:show, :update, :edit, :destroy]
  respond_to :json
  def index
    @memoirs  = current_user.memoirs.order('created_at DESC')
    respond_to do |format|
      format.json { render json: @memoirs }
      format.html
    end
  end

  def show
    gon.memoir = @memoir
    respond_to do |format|
      format.json { render json: @memoir }
      format.html
      format.pdf do
       render :pdf => "file_name",
       :template => 'memoirs/show.pdf.haml',
       page_size:                      'Letter',            # default A4
       page_height:                    297,
       page_width:                     210  ,
       :encoding  => "UTF-8",
       margin:  {   top:               10,                     # default 10 (mm)
                    bottom:            10,
                    left:              0,
                    right:             0 }
     end
    end
  end

  def edit
    gon.memoir = @memoir
  end

  def create
    @memoir = current_user.memoirs.new(memoir_params)
    respond_to do |format|
      if @memoir.save
        format.json { render json: @memoir }
      else
        format.json { render json: @memoir.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @memoir.update(memoir_params)
        format.json { render json: @memoir }
      else
        format.json { render json: @memoir.errors, :status => :unprocessable_entity }
      end
    end
  end


  def destroy
    @memoir.destroy if params[:id] && params[:flag] && params[:flag].eq("true")
    @memoir.educatizations.find_by(education_id: params[:education_id]).destroy if params[:id] && params[:education_id]
    @memoir.experiencizations.find_by(experience_id: params[:experience_id]).destroy if params[:id] && params[:experience_id]
    respond_to do |format|
      format.json { render json: @memoir }
    end
  end
  private
    def set_memoir
      @memoir = Memoir.find(params[:id])
    end

    def memoir_params
      params.require(:memoir).permit(:lang, :user_id, :address_id, :hobbies, :name, :experience_id, :education_id)
    end
end
