class EducationsController < ApplicationController
  before_action :set_education, only: [:show, :update, :destroy]
  def index
    #binding.pry
    if params[:memoir_id] && params[:b] == 'true'
      memoir = Memoir.find(params[:memoir_id])
      @educations = memoir.educations.order("created_at DESC")
    elsif  params[:memoir_id] && params[:b] == 'false'
      @educations = current_user.educations.order("created_at DESC").select { |u| !u.memoir_ids.include?(params[:memoir_id]) }
    end
    respond_to do |format|
      format.json { render json: @educations }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @education }
      format.html
    end
  end

  def create
    @education = current_user.educations.new(education_params)
    respond_to do |format|
      if @education.save
        format.json { render json: @education }
      else
        format.json { render json: @education.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    #binding.pry
    respond_to do |format|
      if @education.update(memoir_ids: params[:memoir_id][:id])
        format.json { render json: @education }
      else
        format.json { render json: @education.errors, :status => :unprocessable_entity }
      end
    end
  end
  def destroy
    @education.destroy
    respond_to do |format|
      format.json { render json: @education }
    end
  end
  private
    def set_education
      @education = Education.find(params[:id])
    end

    def education_params
      params.require(:education).permit(:start, :finish, :school, :proffesion, :speciality, :user_id, :memoir_ids)
    end
end
