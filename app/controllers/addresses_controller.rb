class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]
  def index
    @addresses = current_user.addresses.order('created_at DESC')
    respond_to do |format|
      format.json { render json: @addresses }
      format.html
    end
  end
  def show
    respond_to do |format|
      format.json { render json: @address }
      format.html
    end
  end
  def create
    @address = current_user.addresses.new(address_params)
    respond_to do |format|
      if @address.save
        format.json { render json: @address }
      else
        format.json { render json: @address.errors, :status => :unprocessable_entity }
      end
    end
  end
  def update
    respond_to do |format|
      if @address.update(address_params)
        format.json { render json: @address }
      else
        format.json { render json: @address.errors, :status => :unprocessable_entity }
      end
    end
  end
  def destroy
    @address.destroy
    respond_to do |format|
      format.json { render json: @address }
    end
  end
  private
    def set_address
      @address = Address.find(params[:id])
    end

    def address_params
      params.require(:address).permit(:type, :street, :housenumber, :flatnumber, :postalcode, :city, :country, :user_id)
    end
end
